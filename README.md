# Aalto Machine #1: Pilvi the Plotter

We decided to build a 2D plotter that uses the CoreXY setup for the motors.

[![Image Placeholder](images/pilvitheplotter.jpg)](./videos/pilvitheplotter.mov)

## Authors and their main roles

- [Matti Niinimäki](https://fabacademy.org/2022/labs/aalto/students/matti-niinimaki/) (Fab Academy student)
    - Overall machine design
    - Software configuration
    - Electronics
- [Arthur Tollet](https://fabacademy.org/2022/labs/aalto/students/arthur-tollet/) (Fab Academy student)
    - Limit switch brackets
- [Darren Bratten](https://darrenbratten.gitlab.io/digital-fabrication_2022/) (Aalto Digital Fabrication course student)
    - Designing and fabricating the pen mechanism
- [Yikun Wang](https://yikun_wang.gitlab.io/digital-fabrication/post/mechanical-design/) (Aalto Digital Fabrication course student)
    - 3D model of the entire machine
    - Designing and fabricating the frame, chassis and belt pulleys
- [Yoona Yang](https://yyoona.gitlab.io/digital-fabrication/) (Aalto Digital Fabrication course student)
    - Designing and fabricating the paper holder/carrying plate

## License

MIT License

## Documentation

### Benchmarking

We examined the AxiDraw plotter in order to understand hoiw it was built and how the CoreXY setup works.

### First prototype

Firstly, we want to quickly build the model with the makeblock beams we had available, to figure out how the machanism works.

After this testing we were confident that we have all the parts needed to build the machine and we started to replace the Makeblock parts with 3D printed or laser cut parts that we fabricated ourselves.

### Second prototype

This version had the X and Y axis movements working, but the pen was permanently fixed in the down position. This version allowed us to test the electronics, different grbl versions, as well as start on configuring the Gcode settings for the machine.

#### Problems identified at this stage

At this point, we had some issues with the machine. It would randomly just pause while running a job. At first, I thought we were sending some incompatible commands, but it didn't seem to be associated with any specific line. It seemed to happen quite randomly.

The other hypothesis was that perhaps our power supply was not able to provide enough current. We used a 12V/3A power supply and the motors were rated for 1.68A (we had configured the stepper drives a little lower than this). We changed the power supply to a 19V/4.7A one but it did not solve the issue.

**Later we found out that the issues seemed to be with the version of grbl. Updating to version 1.1 seemed fix this freezing issue.**

### Third prototype

We built a prototype of the pen mechanism using parts that we had available. This allowed us to test the proper grbl version with servo support. The sled that held the pen in place was a little bit loose so the pen would wobble quite a lot. Everything worked though!

Darren was hard at work designing a new pen holder mechanism to replace the wobbly one.

### Testing the electronics

* [Setting the stepper motor current limit](https://discuss.inventables.com/t/setting-the-stepper-motor-current-limit/14998)
![G shield test points for reference voltage](.images/G_shield.jpg)
The gShield (version 5) uses a 0.1 ohm current sense resistor so the formula is **Vref = 0.8 * I**

![stepper motor specification](.images/step_motor_specification.png)
The stepper motor we are using has a maximum current rating of **1.68 amps** per phase. To set the maximum current limit for this motor, we need to set the potentiometer to **(0.8 * 1.68 amps) = 1.34 volts**.

### Part List

| Amount | Part |
| :---: | :--- |
| 1 | [Arduino Uno](https://www.digikey.fi/fi/products/detail/arduino/A000066/2784006) |
| 1 | [Synthetos gShield (grblShield) V5](https://www.digikey.fi/fi/products/detail/adafruit-industries-llc/1750/5054480) |
| 1 | Power Supply (we used 19V/4.7A) |
| 2 | NEMA-17 Stepper Motor (We used M42STH47-1684S but any decent stepper will work) |
| 1 | [9g Micro Servo (TowerPro SG90 or similar)](https://www.digikey.fi/fi/products/detail/adafruit-industries-llc/169/5154651) |
| 4 | [Linear Bearing Platform (Small) - 8mm Diameter - SC8UU](https://www.digikey.fi/fi/products/detail/adafruit-industries-llc/1179/5356859) |
| 2 | [Linear Bearing Platform (Large) - 8mm Diameter - SC8LUU](https://www.digikey.fi/fi/products/detail/adafruit-industries-llc/1180/5356860) |
| 4 | [D8x496mm Linear Motion Shaft](https://www.studica.com/Makeblock-Linear-Motion-Shaft-D8x496mm) |
| 2 | [Stepper Motor Mount with Hardware - NEMA-17](https://www.digikey.fi/fi/products/detail/adafruit-industries-llc/1297/5353638) |
| 4 | [6x150mm shafts (or bolts, can be shorter also)](https://www.servocity.com/6mm-x-150mm-stainless-steel-precision-shafting/) |
| 4 | 4x80mm shafts (for the middle belt pulleys) |
| 3 | 4x80mm shafts (for the pen holder mechanism) |
| 10 | 4x8x3mm bearings (for the belt pulleys) |
| 16 | 4mm shaft collars |
| 8 | 6mm shaft collars |
| 2 | GT2/18T belt pulley |
| 1 | 2m GT2 belt |
| 1 | Makeblock Belt Connector (one set of two) |

### Laser Cut Parts

| Amount | Part | Download |
| :---: | :--- | :--- |
| 1 | Bottom Middle Chassis | link |
| 1 | Top Middle Chassis | link |
| 1 | Y+ Frame | link |

### 3D Printed Parts

| Amount | Part | Download |
| :---: | :--- | :--- |
| 5 | 3D Printed Pulley | link |
| 1 | Left Side Frame | link |
| 1 | Right Side Frame | link |

### Software

#### grbl

In order to be able to send G-Code to the plotter, we need to upload a special G-Code interpreter on the Arduino Uno board. Grbl is an open-source project that does exactly that. However, the standard grbl/g-code does not support servo motors like we are using for the pen up/down control. 

We need to use a specific version of grbl that sends servo PWM commands on pin 11 of the Arduino Uno. Gladly, there are friendly people on the internet that have done this for us.

1. [Download Grbl-Pen-Servo](https://github.com/bdring/Grbl_Pen_Servo)
2. Install it as an Arduino Library (copy the grbl folder to your)
3. Edit the ```config.h``` file of the library and find the line that says:  ```#define COREXY``` and uncomment that line.
4. You might also need to edit the ```spindle_control.c``` file to adjust the servo positions (more on this later).
5. Save all edited files.
6. Open the example code grblUpload from the library's examples in Arduino IDE and upload it to your board.

#### Universal GCode Sender

[Download Universal GCode Sender](https://winder.github.io/ugs_website/)

These settings will depend on the step count of your motors and the stepper motor controller you are using and how you have configured the microstepping mode of it.

The settings for Pilvi are as follows:

| Command | Value | Explanation |
| :---: | :---: | :--- |
| $0    | 10         |       Step pulse time
| $1    | 25         |	     Step idle delay
| $2    | 0          |	     Step pulse invert
| $3    | 0          |	     Step direction invert
| $4    | 0          |	     Invert step enable pin
| $5    | 0          |	     Invert limit pins
| $6    | 0	         |       Invert probe pin
| $10	| 1	         |       Status report options
| $11	| 0.010	     |       Junction deviation
| $12	| 0.002	     |       Arc tolerance
| $13	| 0	         |       Report in inches
| $20	| 0	         |       Soft limits enable
| $21	| 0	         |       Hard limits enable
| $22	| 0	         |       Homing cycle enable
| $23	| 0	         |       Homing direction invert
| $24	| 25.000	 |       Homing locate feed rate
| $25	| 500.000	 |       Homing search seek rate
| $26  |  250	     |       Homing switch debounce delay
| $27  |  1.000	     |       Homing switch pull-off distance
| $30  |  1000	     |       Maximum spindle speed
| $31  |  0	         |       Minimum spindle speed
| $32  |  0	         |       Laser-mode enable
| $100 |  52.000	 |       X-axis travel resolution
| $101 |  52.000	 |       Y-axis travel resolution
| $102 |  52.000	 |       Z-axis travel resolution
| $110 |  3000.000	 |       X-axis maximum rate
| $111 |  3000.000	 |       Y-axis maximum rate
| $112 |  3000.000	 |       Z-axis maximum rate
| $120 |  1000.000	 |       X-axis acceleration
| $121 |  1000.000	 |       Y-axis acceleration
| $122 |  1000.000	 |       Z-axis acceleration
| $130 |  200.000	 |       X-axis maximum travel
| $131 |  200.000	 |       Y-axis maximum travel
| $132 |  200.000	 |       Z-axis maximum travel

#### CNCjs

We could also use UGS to send the plotter files, but I prefer to use another tool after I have tested and configured the gcode settings. We are using [CNCjs](https://cnc.js.org/) to control the machine.

#### Drawingbot V3

To create the Gcode, we use [Drawingbot V3](https://drawingbotv3.readthedocs.io/en/latest/about.html)

